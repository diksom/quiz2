Essay A

<?php
function hitung($string_data)
{
    $operator = array();

    for ($i = 0; $i <= strlen($string_data) - 1; $i++) {
        if (($string_data[$i] == "+") || ($string_data[$i] == "-") || ($string_data[$i] == "*") || ($string_data[$i] == ":") || ($string_data[$i] == "%")) {
            $operator[] = $string_data[$i];
        }
    }

    $string_data = str_replace("+", " ", $string_data);
    $string_data = str_replace("-", " ", $string_data);
    $string_data = str_replace("*", " ", $string_data);
    $string_data = str_replace(":", " ", $string_data);
    $string_data = str_replace("%", " ", $string_data);

    $operand = explode(" ", $string_data);

    $hasil = $operand[0];

    for ($i = 0; $i <= count($operator) - 1; $i++) {
        if ($operator[$i] == "+") $hasil = $hasil + $operand[$i + 1];
        else if ($operator[$i] == "-") $hasil = $hasil - $operand[$i + 1];
        else if ($operator[$i] == "*") $hasil = $hasil * $operand[$i + 1];
        else if ($operator[$i] == ":") $hasil = $hasil / $operand[$i + 1];
        else if ($operator[$i] == "%") $hasil = $hasil % $operand[$i + 1];
    }

    echo "Hasil perhitungannya adalah : " . $hasil . "<br>";
}
echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");
?>

Essay B

<?php
function perolehan_medali($arrayMedali)
{
    $arrayMedali = array(
        array('Indonesia',  'emas',),
        array('India',  'perak'),
        array('Korea Selatan',  'emas',),
        array('India', 'perak'),
        array('India', 'emas'),
        array('Indonesia', 'perak'),
        array('Indonesia', 'emas')
    );
    
    return $arrayMedali;
}
print_r(perolehan_medali([]));
?>


Essay C
CREATE TABLE `customers`(
`id` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(255) NOT NULL,
`email` VARCHAR(255) NOT NULL,
`password` VARCHAR(255) NOT NULL,
PRIMARY KEY(`id`)
) ENGINE = INNODB;

CREATE TABLE `orders`(
`id` INT NOT NULL AUTO_INCREMENT,
`amount` VARCHAR(255) NOT NULL,
`customer_id` INT NOT NULL,
PRIMARY KEY(`id`),
FOREIGN KEY(`customer_id`) REFERENCES customers(`id`));

Essay D
INSERT INTO `customers`(`id`, `name`, `email`, `password`)
VALUES(NULL,'Jhon Doe','john@doe.com','john123'),
(NULL,'Jane Doe','jane@doe.com','jenita123');
INSERT INTO `orders` (`id`, `amount`, `customer_id`) VALUES (NULL, '500', '1'), (NULL, '200', '2'), (NULL, '750', '2'), (NULL, '250', '1'), (NULL, '400', '2');

Essay E
select customers.name as customer_name, SUM(orders.amount) as total_amount FROM customers,orders where customers.id=orders.customer_id GROUP by orders.customer_id;